def tri_insertion(tableau):
    for i in range(1,len(tableau)):
        valeur=tableau[i]
        j=i
        while j>0 and tableau[j-1]>valeur:
            tableau[j]=tableau[j-1]
            j=j-1
        tableau[j]=valeur
    return tableau
